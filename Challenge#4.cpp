#include<iostream>
#include<cstdlib>
#include<vector>
#include<ctime>
#include<cmath>

using namespace std;


void sumofallelement(int &sum, vector<unsigned int> &collect, int i);
void sumelementpow2(int &sumpow, vector<unsigned int> &collect, int i);
void swap(vector<unsigned int> &collect, int i);
void sorting(vector<unsigned int> &collect, int scope);
void highestvalue(vector<unsigned int> &collect, int &highest, int scope);
void lowestvalue(vector<unsigned int> &collect, int &lowest, int scope);
void meanvalue(int sum, double &avg, int scope);
void medianvalue(vector<unsigned int> &collect, int sum, double &med, int scope);
void modevalue(vector<unsigned int> &collect, int sum, int &mode, int countmode[101], int scope, int &number);
void stddeviation(vector<unsigned int> &collect, int scope, int sum, int sumpow, double &sd);
void evennumbers(vector<unsigned int> &collect, int &evennum, int scope);
void oddnumbers(vector<unsigned int> &collect, int &oddnum, int scope);
void outputnumber(vector<unsigned int> &collect, int scope);
void Displayresult(vector<unsigned int> &collect, int count, int sum, int highest, int lowest, double avg, double med, int number, double sd, int evennum, int oddnum);

int main() {
	srand(time(0));
	int scope = 0, count = 0, sum = 0, highest = 0, lowest = 0, mode = 0, evennum = 0, oddnum = 0, sumpow = 0, number = 0;
	scope = rand() % 100 + 50;
	double med = 0, sd = 0, avg = 0;
	vector<unsigned int> collect = {}; // random scope of vector in order to pushback in vector
	int countmode[101] = {};
	for (int i = 0; i < scope; i++) { // O(N) random number between 0-100
		collect.push_back(rand() % 101);
		sumofallelement(sum, collect, i);
		sumelementpow2(sumpow, collect, i);
		count++;
	}
	cout << "Result of random value: " << endl;
	outputnumber(collect, scope);
	sorting(collect, scope);
	highestvalue(collect, highest, scope);
	lowestvalue(collect, lowest, scope);
	meanvalue(sum, avg, scope);
	medianvalue(collect, sum, med, scope);
	modevalue(collect, sum, mode, countmode, scope, number);
	evennumbers(collect, evennum, scope);
	oddnumbers(collect, oddnum, scope);
	stddeviation(collect, scope, sum, sumpow, sd);
	Displayresult(collect, count, sum, highest, lowest, avg, med, number, sd, evennum, oddnum);
	cout << "The values output in order from lowest to highest: " << endl;
	outputnumber(collect, scope);
	return 0;
}

void sorting(vector<unsigned int> &collect, int scope) { // O(N^2) sorting value in vector 
	int check;
	do {
		check = 0;
		for (int i = 0; i < scope - 1; i++) {
			if (collect[i + 1] < collect[i]) {
				swap(collect, i);
				check = 1;
			}
		}
	} while (check == 1);
}


void sumofallelement(int &sum, vector<unsigned int> &collect, int i) { //O(1) Find sum of all values
	sum = sum + collect[i];
}

void swap(vector<unsigned int> &collect, int i) { //O(1) Swap integer
	int hold = 0;
	hold = collect[i + 1];
	collect[i + 1] = collect[i];
	collect[i] = hold;
}

void highestvalue(vector<unsigned int> &collect, int &highest, int scope) { //O(N) Find highest of value
	highest = collect[0];
	for (int i = 0; i < scope - 1; i++) {
		if (collect[i + 1] > collect[i]) {
			highest = collect[i + 1];
		}
		else {
			highest = highest;
		}
	}
}

void lowestvalue(vector<unsigned int> &collect, int &lowest, int scope) { //O(N) Find lowest of value
	lowest = collect[0];
	for (int i = 0; i < scope - 1; i++) {
		if (collect[i + 1] < collect[i]) {
			lowest = collect[i + 1];
		}
		else {
			lowest = lowest;
		}
	}
}

void meanvalue(int sum, double &avg, int scope) { // O(1) Find average(mean) of value
	avg = (double) sum / scope;
}

void medianvalue(vector<unsigned int> &collect, int sum, double &med, int scope) { // O(1) Find median of value
	double high = 0, low = 0,plus = 0;
	if (scope % 2 == 0) {
		med = (scope + 2) / 2;
	}
	else {
		high = (scope / 2.0) + 0.5;
		low = (scope / 2.0) - 0.5;
		plus = collect[high] + collect[low];
		med = plus / 2;
	}
}

void modevalue(vector<unsigned int> &collect, int sum, int &mode, int countmode[101], int scope, int &number) { // O(N^2) Find mode of value
	int counthighest = 0;
	for (int i = 0; i < scope; i++) {
		for (int j = 0; j <= 100; j++) {
			if (collect[i] == j) {
				countmode[j]++;
			}
		}
	}
	for (int i = 0; i <= 100; i++) {
		if (counthighest < countmode[i]) {
			counthighest = countmode[i];
			number = i;
		}
		else {
			counthighest = counthighest;
		}
	}
}

void evennumbers(vector<unsigned int> &collect, int &evennum, int scope) { // O(N) Find Even numbers 
	for (int i = 0; i < scope; i++) {
		if (collect[i] % 2 == 0) {
			evennum++;
		}
	}
}

void oddnumbers(vector<unsigned int> &collect, int &oddnum, int scope) { // O(N) Find Odd numbers 
	for (int i = 0; i < scope; i++) {
		if (collect[i] % 2 != 0) {
			oddnum++;
		}
	}
}

void outputnumber(vector<unsigned int> &collect, int scope) { // O(N) Display the values 
	for (int i = 0; i < scope; i++) {
		cout << " " << collect[i] ;
		if (i != 0) {
			if (i % 35 == 0) {
				cout << endl;
			}
		}
	}
	cout << endl;
}

void stddeviation(vector<unsigned int> &collect, int scope, int sum, int sumpow, double &sd) { // O(1)  Find sd from scope and sumpow 
	sd = sqrt(((scope*sumpow) - sum) / (scope*(scope - 1)));
}

void sumelementpow2(int &sumpow, vector<unsigned int> &collect, int i) { //O(1) Each element power 2 and bring to plus together instance. 1^2 + 2^2 + 3^2 = sumpow Find sumpow 
	sumpow = sumpow + ((collect[i]) ^ 2);
}

void Displayresult(vector<unsigned int> &collect, int count, int sum, int highest, int lowest, double avg, double med, int number, double sd, int evennum, int oddnum) { //O(1) Display result 
	cout << "The number of elements: " << count << endl;
	cout << "The sum of all elements: " << sum << endl;
	cout << "The highest value: " << highest << endl;
	cout << "The lowest value: " << lowest << endl;
	cout << "The mean value: " << avg << endl;
	cout << "The median value: " << med << endl;
	cout << "The mode value: " << collect[number] << endl;
	cout << "The standard deviation: " << sd << endl;
	cout << "The number of even numbers: " << evennum << endl;
	cout << "The number of odd numbers: " << oddnum << endl;
}